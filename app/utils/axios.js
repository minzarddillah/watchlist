import axios from 'axios';
import {API_URL, API_KEY} from '@env';

console.log(API_URL);

axios.defaults.baseURL = API_URL;
axios.defaults.timeout = 35000;
axios.defaults.params = {};
axios.defaults.params.api_key = API_KEY;
axios.defaults.params.language = 'en-US';
axios.interceptors.request.use(
  async response => {
    const originalConfig = response;

    originalConfig.headers['Access-Control-Allow-Origin'] = '*';

    return originalConfig;
  },
  error => Promise.reject(error),
);

export default axios;
