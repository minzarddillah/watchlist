import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';

import reducers from '../reducers';

const middleware = [];
middleware.push(thunk);

if (__DEV__) {
  middleware.push(createLogger());
}

const store = createStore(reducers, applyMiddleware(...middleware));

export default store;
