import _ from 'lodash';
import 'intl';
import 'intl/locale-data/jsonp/en';

export const formatSeparator = (number, fixed = 0) => {
  if (number === undefined || _.isEmpty(String(number))) {
    return '0';
  }
  return new Intl.NumberFormat('en', {
    minimumFractionDigits: fixed,
  }).format(Number(number || '0').toFixed(fixed));
};
