export default {
  Black: '#000',
  White: '#FFF',
  Tundora: '#444444',
  CodGray: '#1C1C1C',
  MineShaft: '#252525',
  Monza: '#E50914',
  Mercury: '#E2E2E2',
  Silver: '#C0C0C0',
  Green: '#00D100',
  Backdrop: 'rgba(0, 0, 0, 0.5)',
  Transparent: 'transparent',
};
