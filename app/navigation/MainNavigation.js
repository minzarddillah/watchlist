import React, {useEffect, useRef} from 'react';
import {NavigationContainer, DarkTheme} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
// import SplashScreen from 'react-native-splash-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {connect} from 'react-redux';

import color from '../utils/color';
import {setSaved} from '../actions/setupAction';
import BottomStack from './BottomStack';

import ListMovie from '../screens/ListMovie/ListMovie.container';
import Detail from '../screens/Detail/Detail.container';
import Search from '../screens/Search/Search.container';

const Stack = createStackNavigator();

const MainNavigation = ({setSavedDispatch}) => {
  const routeNameRef = useRef();
  const navigationRef = useRef();

  useEffect(() => {
    AsyncStorage.getItem('saved')
      .then(saved => setSavedDispatch(JSON.parse(saved || '[]')))
      .then(() => {
        // SplashScreen.hide();
      });
  }, [setSavedDispatch]);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        routeNameRef.current = navigationRef.current.getCurrentRoute().name;
      }}
      onStateChange={async () => {
        const currentRouteName = navigationRef.current.getCurrentRoute().name;
        routeNameRef.current = currentRouteName;
      }}
      theme={{
        ...DarkTheme,
        colors: {
          ...DarkTheme.colors,
          primary: color.Black,
          background: color.Black,
          card: color.Black,
          text: color.White,
          border: color.Black,
        },
      }}
    >
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Main"
          component={BottomStack}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ListMovie"
          component={ListMovie}
          options={{headerShown: false, ...TransitionPresets.SlideFromRightIOS}}
        />
        <Stack.Screen
          name="Detail"
          component={Detail}
          options={{
            headerShown: false,
            ...TransitionPresets.ModalSlideFromBottomIOS,
          }}
        />
        <Stack.Screen
          name="Search"
          component={Search}
          options={{headerShown: false, ...TransitionPresets.SlideFromRightIOS}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
  setSavedDispatch: data => setSaved(data, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigation);
