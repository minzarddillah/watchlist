import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Feather';

import HomeScreen from '../screens/Home/Home.container';
import DiscoverScreen from '../screens/Discover/Discover.container';
import color from '../utils/color';

const Tab = createBottomTabNavigator();

const BottomTabBar = () => (
  <Tab.Navigator
    initialRouteName="Home"
    screenOptions={{
      headerShown: false,
      tabBarShowLabel: false,
      tabBarItemStyle: {
        borderTopWidth: 3,
        borderColor: color.CodGray,
      },
      tabBarActiveTintColor: color.Monza,
      tabBarInactiveTintColor: color.White,
    }}>
    <Tab.Screen
      name="Home"
      component={HomeScreen}
      options={{
        tabBarIcon: ({color, size}) => (
          <Icon name="home" size={size} color={color} />
        ),
      }}
    />
    <Tab.Screen
      name="Discover"
      component={DiscoverScreen}
      options={{
        tabBarIcon: ({color, size}) => (
          <Icon name="bookmark" size={size} color={color} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default BottomTabBar;
