import {
  REQUEST_UPCOMING_MOVIES,
  REQUEST_UPCOMING_MOVIES_SUCCESS,
  REQUEST_UPCOMING_MOVIES_FAILED,
  REQUEST_NOW_PLAYING_MOVIES,
  REQUEST_NOW_PLAYING_MOVIES_SUCCESS,
  REQUEST_NOW_PLAYING_MOVIES_FAILED,
  REQUEST_POPULAR_MOVIES,
  REQUEST_POPULAR_MOVIES_SUCCESS,
  REQUEST_POPULAR_MOVIES_FAILED,
  REQUEST_TOP_RATED_MOVIES,
  REQUEST_TOP_RATED_MOVIES_SUCCESS,
  REQUEST_TOP_RATED_MOVIES_FAILED,
} from '../actions/types';

const initialState = {
  loadingUpcoming: false,
  upcomingMovies: [],

  loadingNowPlayingMovies: false,
  nowPlayingMovies: [],

  loadingPopularMovies: false,
  popularMovies: [],

  loadingTopRatedMovies: false,
  topRatedMovies: [],

  featured: [
    {
      backdrop_path: '/wiE9doxiLwq3WCGamDIOb2PqBqc.jpg',
      name: 'Popular TV Shows',
      api: '/tv/popular',
      titleHeader: 'Popular TV Shows',
      testID: 'featuredPopularTVShows',
    },
    {
      backdrop_path: '/wKHImjeHwVIiWJWFIJtWUKkA5QJ.jpg',
      name: 'Top Rated TV Shows',
      api: '/tv/top_rated',
      titleHeader: 'Top Rated TV Shows',
      testID: 'featuredTopRatedTVShows',
    },
    {
      backdrop_path: '/qYUvHyFTmA2zLhpuQsM19aUzpZy.jpg',
      name: 'On the Air TV Shows',
      api: '/tv/on_the_air',
      titleHeader: 'On the Air TV Shows',
      testID: 'featuredOnTheAirTVShows',
    },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_UPCOMING_MOVIES:
      return {
        ...state,
        loadingUpcoming: true,
        upcomingMovies: [],
      };
    case REQUEST_UPCOMING_MOVIES_SUCCESS:
      return {
        ...state,
        loadingUpcoming: false,
        upcomingMovies: action.data,
      };
    case REQUEST_UPCOMING_MOVIES_FAILED:
      return {
        ...state,
        loadingUpcoming: false,
        upcomingMovies: [],
      };
    case REQUEST_NOW_PLAYING_MOVIES:
      return {
        ...state,
        loadingNowPlayingMovies: true,
        nowPlayingMovies: [],
      };
    case REQUEST_NOW_PLAYING_MOVIES_SUCCESS:
      return {
        ...state,
        loadingNowPlayingMovies: false,
        nowPlayingMovies: action.data,
      };
    case REQUEST_NOW_PLAYING_MOVIES_FAILED:
      return {
        ...state,
        loadingNowPlayingMovies: false,
        nowPlayingMovies: [],
      };
    case REQUEST_POPULAR_MOVIES:
      return {
        ...state,
        loadingPopularMovies: true,
        popularMovies: [],
      };
    case REQUEST_POPULAR_MOVIES_SUCCESS:
      return {
        ...state,
        loadingPopularMovies: false,
        popularMovies: action.data,
      };
    case REQUEST_POPULAR_MOVIES_FAILED:
      return {
        ...state,
        loadingPopularMovies: false,
        popularMovies: [],
      };
    case REQUEST_TOP_RATED_MOVIES:
      return {
        ...state,
        loadingTopRatedMovies: true,
        topRatedMovies: [],
      };
    case REQUEST_TOP_RATED_MOVIES_SUCCESS:
      return {
        ...state,
        loadingTopRatedMovies: false,
        topRatedMovies: action.data,
      };
    case REQUEST_TOP_RATED_MOVIES_FAILED:
      return {
        ...state,
        loadingTopRatedMovies: false,
        topRatedMovies: [],
      };
    default:
      return state;
  }
};
