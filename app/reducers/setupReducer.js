import {SET_GENRES, SET_SAVED} from '../actions/types';

const initialState = {
  genres: [],
  saved: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_GENRES:
      return {
        ...state,
        genres: action.data,
      };
    case SET_SAVED:
      return {
        ...state,
        saved: action.data,
      };
    default:
      return state;
  }
};
