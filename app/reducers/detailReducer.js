import {
  REQUEST_DETAIL_MOVIES,
  REQUEST_DETAIL_MOVIES_SUCCESS,
  REQUEST_DETAIL_MOVIES_FAILED,
} from '../actions/types';

const initialState = {
  detail: {},
  cast: [],
  videos: [],
  similiar: [],
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_DETAIL_MOVIES:
      return {
        ...state,
        detail: {},
        cast: [],
        videos: [],
        similiar: [],
        loading: true,
      };
    case REQUEST_DETAIL_MOVIES_SUCCESS:
      return {
        ...state,
        detail: action.data.detail,
        cast: action.data.cast,
        videos: action.data.videos,
        similiar: action.data.similiar,
        loading: false,
      };
    case REQUEST_DETAIL_MOVIES_FAILED:
      return {
        ...state,
        detail: {},
        cast: [],
        videos: [],
        similiar: [],
        loading: false,
      };
    default:
      return state;
  }
};
