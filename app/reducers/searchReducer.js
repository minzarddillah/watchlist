import {
  REQUEST_SEARCH_SUCCESS,
  REQUEST_SEARCH_PAGING_SUCCESS,
  CLEAR_DATA_SEARCH,
} from '../actions/types';

const initialState = {
  page: 1,
  totalPages: 1,
  data: [],
  loading: false,
  loadingPaging: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_SEARCH_SUCCESS:
      return {
        ...state,
        page: 2,
        totalPages: action.data.totalPages,
        data: action.data.results,
        loading: false,
      };
    case REQUEST_SEARCH_PAGING_SUCCESS:
      return {
        ...state,
        loadingPaging: false,
        page: action.data.page,
        data: state.data.concat(action.data.results),
      };
    case CLEAR_DATA_SEARCH: {
      return initialState;
    }
    default:
      return state;
  }
};
