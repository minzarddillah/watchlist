import {
  REQUEST_LIST_MOVIES,
  REQUEST_LIST_MOVIES_SUCCESS,
  REQUEST_LIST_MOVIES_FAILED,
  REQUEST_LIST_MOVIES_PAGING,
  REQUEST_LIST_MOVIES_PAGING_SUCCESS,
  REQUEST_LIST_MOVIES_PAGING_FAILED,
} from '../actions/types';

const initialState = {
  page: 1,
  totalPages: 1,
  data: [],
  loading: false,
  loadingPaging: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_LIST_MOVIES:
      return {
        ...state,
        loading: true,
      };
    case REQUEST_LIST_MOVIES_SUCCESS:
      return {
        ...state,
        page: 2,
        totalPages: action.data.totalPages,
        data: action.data.results,
        loading: false,
      };
    case REQUEST_LIST_MOVIES_FAILED:
      return {
        ...state,
        loading: false,
      };
    case REQUEST_LIST_MOVIES_PAGING:
      return {
        ...state,
        loadingPaging: true,
      };
    case REQUEST_LIST_MOVIES_PAGING_SUCCESS:
      return {
        ...state,
        loadingPaging: false,
        page: action.data.page,
        data: state.data.concat(action.data.results),
      };
    case REQUEST_LIST_MOVIES_PAGING_FAILED:
      return {
        ...state,
        loadingPaging: false,
      };
    default:
      return state;
  }
};
