import {combineReducers} from 'redux';

import homeReducer from './homeReducer';
import setupReducer from './setupReducer';
import listMovieReducer from './listMovieReducer';
import detailReducer from './detailReducer';
import searchReducer from './searchReducer';

export default combineReducers({
  homeReducer,
  setupReducer,
  listMovieReducer,
  detailReducer,
  searchReducer,
});
