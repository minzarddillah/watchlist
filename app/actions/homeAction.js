import axios from '../utils/axios';
import {
  REQUEST_UPCOMING_MOVIES,
  REQUEST_UPCOMING_MOVIES_SUCCESS,
  REQUEST_UPCOMING_MOVIES_FAILED,
  SET_GENRES,
  REQUEST_NOW_PLAYING_MOVIES,
  REQUEST_NOW_PLAYING_MOVIES_SUCCESS,
  REQUEST_NOW_PLAYING_MOVIES_FAILED,
  REQUEST_POPULAR_MOVIES,
  REQUEST_POPULAR_MOVIES_SUCCESS,
  REQUEST_POPULAR_MOVIES_FAILED,
  REQUEST_TOP_RATED_MOVIES,
  REQUEST_TOP_RATED_MOVIES_SUCCESS,
  REQUEST_TOP_RATED_MOVIES_FAILED,
} from './types';

export const getUpcomingMovies = async dispatch => {
  dispatch({
    type: REQUEST_UPCOMING_MOVIES,
  });

  try {
    const {data} = await axios.get('/movie/upcoming');
    dispatch({
      type: REQUEST_UPCOMING_MOVIES_SUCCESS,
      data: data.results.slice(0, 5),
    });
  } catch (error) {
    // alert('error')
    console.log('hello world');
    console.log(JSON.stringify(error));
    dispatch({
      type: REQUEST_UPCOMING_MOVIES_FAILED,
      error: error.response,
    });
  }
};

export const getGenres = async dispatch => {
  try {
    const [genreMovie, genreTv] = await Promise.all([
      axios.get('/genre/movie/list'),
      axios.get('/genre/tv/list'),
    ]);
    const genres = [...genreMovie.data.genres, ...genreTv.data.genres];
    const result = Array.from(new Set(genres.map(s => s.id))).map(id => ({
      id,
      name: genres.find(s => s.id === id).name,
    }));
    dispatch({
      type: SET_GENRES,
      data: result,
    });
  } catch (error) {}
};

export const getNowPlayingMovies = async dispatch => {
  dispatch({
    type: REQUEST_NOW_PLAYING_MOVIES,
  });

  try {
    const {data} = await axios.get('/movie/now_playing');
    dispatch({
      type: REQUEST_NOW_PLAYING_MOVIES_SUCCESS,
      data: data.results,
    });
  } catch (error) {
    dispatch({
      type: REQUEST_NOW_PLAYING_MOVIES_FAILED,
      error: error.response,
    });
  }
};

export const getPopularMovies = async dispatch => {
  dispatch({
    type: REQUEST_POPULAR_MOVIES,
  });

  try {
    const {data} = await axios.get('/movie/popular');
    dispatch({
      type: REQUEST_POPULAR_MOVIES_SUCCESS,
      data: data.results,
    });
  } catch (error) {
    dispatch({
      type: REQUEST_POPULAR_MOVIES_FAILED,
      error: error.response,
    });
  }
};

export const getTopRatedMovies = async dispatch => {
  dispatch({
    type: REQUEST_TOP_RATED_MOVIES,
  });

  try {
    const {data} = await axios.get('/movie/top_rated');
    dispatch({
      type: REQUEST_TOP_RATED_MOVIES_SUCCESS,
      data: data.results,
    });
  } catch (error) {
    dispatch({
      type: REQUEST_TOP_RATED_MOVIES_FAILED,
      error: error.response,
    });
  }
};
