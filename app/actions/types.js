/* eslint-disable prettier/prettier */
export const SET_GENRES = 'SET_GENRES';
export const REQUEST_UPCOMING_MOVIES = 'REQUEST_UPCOMING_MOVIES';
export const REQUEST_UPCOMING_MOVIES_FAILED = 'REQUEST_UPCOMING_MOVIES_FAILED';
export const REQUEST_UPCOMING_MOVIES_SUCCESS = 'REQUEST_UPCOMING_MOVIES_SUCCESS';
export const REQUEST_NOW_PLAYING_MOVIES = 'REQUEST_NOW_PLAYING_MOVIES';
export const REQUEST_NOW_PLAYING_MOVIES_SUCCESS = 'REQUEST_NOW_PLAYING_MOVIES_SUCCESS';
export const REQUEST_NOW_PLAYING_MOVIES_FAILED = 'REQUEST_NOW_PLAYING_MOVIES_FAILED';
export const REQUEST_POPULAR_MOVIES = 'REQUEST_POPULAR_MOVIES';
export const REQUEST_POPULAR_MOVIES_SUCCESS = 'REQUEST_POPULAR_MOVIES_SUCCESS';
export const REQUEST_POPULAR_MOVIES_FAILED = 'REQUEST_POPULAR_MOVIES_FAILED';
export const REQUEST_TOP_RATED_MOVIES = 'REQUEST_TOP_RATED_MOVIES';
export const REQUEST_TOP_RATED_MOVIES_SUCCESS = 'REQUEST_TOP_RATED_MOVIES_SUCCESS';
export const REQUEST_TOP_RATED_MOVIES_FAILED = 'REQUEST_TOP_RATED_MOVIES_FAILED';
export const REQUEST_LIST_MOVIES = 'REQUEST_LIST_MOVIES';
export const REQUEST_LIST_MOVIES_SUCCESS = 'REQUEST_LIST_MOVIES_SUCCESS';
export const REQUEST_LIST_MOVIES_FAILED = 'REQUEST_LIST_MOVIES_FAILED';
export const REQUEST_LIST_MOVIES_PAGING = 'REQUEST_LIST_MOVIES_PAGING';
export const REQUEST_LIST_MOVIES_PAGING_SUCCESS = 'REQUEST_LIST_MOVIES_PAGING_SUCCESS';
export const REQUEST_LIST_MOVIES_PAGING_FAILED = 'REQUEST_LIST_MOVIES_PAGING_FAILED';
export const REQUEST_DETAIL_MOVIES = 'REQUEST_DETAIL_MOVIES';
export const REQUEST_DETAIL_MOVIES_SUCCESS = 'REQUEST_DETAIL_MOVIES_SUCCESS';
export const REQUEST_DETAIL_MOVIES_FAILED = 'REQUEST_DETAIL_MOVIES_FAILED';
export const REQUEST_SEARCH_SUCCESS = 'REQUEST_SEARCH_SUCCESS';
export const REQUEST_SEARCH_PAGING_SUCCESS = 'REQUEST_SEARCH_PAGING_SUCCESS';
export const SET_SAVED = 'SET_SAVED';
export const CLEAR_DATA_SEARCH = 'CLEAR_DATA_SEARCH';
