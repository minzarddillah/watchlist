import axios from '../utils/axios';
import {
  REQUEST_LIST_MOVIES,
  REQUEST_LIST_MOVIES_SUCCESS,
  REQUEST_LIST_MOVIES_FAILED,
  REQUEST_LIST_MOVIES_PAGING,
  REQUEST_LIST_MOVIES_PAGING_SUCCESS,
  REQUEST_LIST_MOVIES_PAGING_FAILED,
} from './types';

export const requestListMovie = async (apiUrl, dispatch) => {
  dispatch({
    type: REQUEST_LIST_MOVIES,
  });
  try {
    const {data} = await axios.get(apiUrl);

    dispatch({
      type: REQUEST_LIST_MOVIES_SUCCESS,
      data: {
        results: data.results,
        totalPages: data.total_pages,
      },
    });
  } catch (error) {
    dispatch({
      type: REQUEST_LIST_MOVIES_FAILED,
      error: error.response,
    });
  }
};

export const requestListMoviePaging = async ({apiUrl, page}, dispatch) => {
  dispatch({
    type: REQUEST_LIST_MOVIES_PAGING,
  });

  try {
    const {data} = await axios.get(`${apiUrl}?page=${page}`);
    dispatch({
      type: REQUEST_LIST_MOVIES_PAGING_SUCCESS,
      data: {
        page: page + 1,
        results: data.results,
      },
    });
  } catch (error) {
    dispatch({
      type: REQUEST_LIST_MOVIES_PAGING_FAILED,
      error: error.response,
    });
  }
};
