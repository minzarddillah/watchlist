import axios from '../utils/axios';
import {
  REQUEST_SEARCH_SUCCESS,
  REQUEST_SEARCH_PAGING_SUCCESS,
  CLEAR_DATA_SEARCH,
} from './types';

export const search = (keyword, dispatch) =>
  new Promise(async (resolve, reject) => {
    try {
      const {data} = await axios.get(`/search/multi?query=${keyword}`);
      const filterData = data.results.filter(
        e => e.media_type !== 'person' && !!e.poster_path,
      );
      dispatch({
        type: REQUEST_SEARCH_SUCCESS,
        data: {
          results: filterData,
          totalPages: data.total_pages,
        },
      });
      resolve();
    } catch (error) {
      reject();
    }
  });

export const requestSearchPaging = async ({keyword, page}, dispatch) =>
  new Promise(async (resolve, reject) => {
    try {
      const {data} = await axios.get(
        `/search/multi?query=${keyword}&page=${page}`,
      );
      const filterData = data.results.filter(
        e => e.media_type !== 'person' && !!e.poster_path,
      );

      dispatch({
        type: REQUEST_SEARCH_PAGING_SUCCESS,
        data: {
          page: page + 1,
          results: filterData,
        },
      });
      resolve();
    } catch (error) {
      reject();
    }
  });

export const clearDataSearch = dispatch => {
  dispatch({
    type: CLEAR_DATA_SEARCH,
  });
};
