import AsyncStorage from '@react-native-async-storage/async-storage';
import {SET_SAVED} from './types';

export const setSaved = (data, dispatch) => {
  AsyncStorage.setItem('saved', JSON.stringify(data));
  dispatch({
    type: SET_SAVED,
    data,
  });
};
