import axios from '../utils/axios';
import {
  REQUEST_DETAIL_MOVIES,
  REQUEST_DETAIL_MOVIES_SUCCESS,
  REQUEST_DETAIL_MOVIES_FAILED,
} from './types';

export const getDetail = ({type, id}, dispatch) => new Promise(async (resolve, reject) => {
  dispatch({
    type: REQUEST_DETAIL_MOVIES,
  });

  try {
    const [detail, credits, videos, similiar] = await Promise.all([
      axios.get(`/${type}/${id}`),
      axios.get(`/${type}/${id}/credits`),
      axios.get(`/${type}/${id}/videos`),
      axios.get(`/${type}/${id}/similar`),
    ]);

    dispatch({
      type: REQUEST_DETAIL_MOVIES_SUCCESS,
      data: {
        detail: detail.data,
        cast: credits.data.cast.filter(e => !!e.profile_path),
        videos: videos?.data?.results?.slice?.(0, 10) || [],
        similiar: similiar.data.results,
      },
    });
    resolve({
      detail: detail.data,
      cast: credits.data.cast.filter(e => !!e.profile_path),
      videos: videos?.data?.results?.slice?.(0, 10) || [],
      similiar: similiar.data.results,
    });
  } catch (error) {
    dispatch({
      type: REQUEST_DETAIL_MOVIES_FAILED,
      error: error.response,
    });
    reject();
  }
});
