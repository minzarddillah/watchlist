import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';

const styles = StyleSheet.create({
  seperator: {
    height: 20,
  },
  loadingPagination: {
    marginVertical: 20,
  },
  wrapper: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? getStatusBarHeight() : 0,
  },
  flexGrow: {
    flexGrow: 1,
  },
});

export default styles;
