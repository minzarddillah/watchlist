import {connect} from 'react-redux';

import ListMovie from './ListMovie.screen';
import {
  requestListMovie,
  requestListMoviePaging,
} from '../../actions/listMovieAction';

const mapStateToProps = state => ({
  page: state.listMovieReducer.page,
  totalPages: state.listMovieReducer.totalPages,
  data: state.listMovieReducer.data,
  loading: state.listMovieReducer.loading,
  genres: state.setupReducer.genres,
  loadingPaging: state.listMovieReducer.loadingPaging,
});

const mapDispatchToProps = dispatch => ({
  requestListMovie: apiUrl => requestListMovie(apiUrl, dispatch),
  requestListMoviePaging: params => requestListMoviePaging(params, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ListMovie);
