import React, {useEffect} from 'react';
import {View, FlatList, ActivityIndicator, SafeAreaView} from 'react-native';

import styles from './ListMovie.style';
import color from '../../utils/color';
import CardMovie from '../../components/CardMovie/CardMovie.component';
import Header from '../../components/Header/Header.component';
import Empty from '../../components/Empty/Empty.component';

const ListMovie = ({
  navigation,
  route,
  requestListMovie,
  page,
  data,
  loading,
  genres,
  loadingPaging,
  requestListMoviePaging,
}) => {
  useEffect(() => {
    navigation.setOptions({title: route?.params?.titleHeader});
    requestListMovie(route?.params?.pathApiUrl);
  }, [navigation, requestListMovie, route]);

  const onEndReached = () => {
    if (loadingPaging) {
      return true;
    }

    requestListMoviePaging({
      apiUrl: route?.params?.pathApiUrl,
      page,
    });
  };

  const ListEmptyComponent = () => <Empty loading={loading} />;

  const renderItem = ({item}) => (
    <CardMovie item={item} genres={genres} type={route?.params?.type} />
  );

  return (
    <SafeAreaView style={styles.wrapper}>
      <Header title={route?.params?.titleHeader || ''} />
      <FlatList
        data={loading ? [] : data?.filter(({backdrop_path}) => !!backdrop_path)}
        keyExtractor={(item, index) => index.toString()}
        renderItem={renderItem}
        onEndReachedThreshold={0.2}
        onEndReached={onEndReached}
        ListEmptyComponent={ListEmptyComponent}
        contentContainerStyle={styles.flexGrow}
        ListFooterComponent={() => {
          return loadingPaging ? (
            <ActivityIndicator
              size="large"
              color={color.White}
              style={styles.loadingPagination}
            />
          ) : (
            <View style={styles.seperator} />
          );
        }}
      />
    </SafeAreaView>
  );
};

export default ListMovie;
