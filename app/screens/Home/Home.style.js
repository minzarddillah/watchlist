import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';

import color from '../../utils/color';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? getStatusBarHeight() : 0,
  },
  titleSection: {
    color: color.White,
    fontSize: 20,
    fontWeight: '600',
  },
  containerTitleSection: {
    paddingHorizontal: 20,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  scrollviewHorizontal: {
    paddingHorizontal: 20,
    marginBottom: 30,
  },

  genre: {
    backgroundColor: color.Monza,
    marginRight: 14,
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
  },
  genreName: {
    color: color.White,
    fontWeight: 'bold',
  },

  containerFeatured: {
    width: 260,
    aspectRatio: 16 / 9,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    marginRight: 15,
  },
  imageFeatured: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    backgroundColor: color.White,
  },
  shadowFeatured: {
    backgroundColor: color.Black,
    opacity: 0.3,
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  nameFeatured: {
    fontSize: 18,
    color: color.White,
    fontWeight: 'bold',
  },
});

export default styles;
