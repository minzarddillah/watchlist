import {connect} from 'react-redux';

import Home from './Home.screen';
import {
  getUpcomingMovies,
  getGenres,
  getNowPlayingMovies,
  getPopularMovies,
  getTopRatedMovies,
} from '../../actions/homeAction';

const mapStateToProps = state => ({
  loadingUpcoming: state.homeReducer.loadingUpcoming,
  upcomingMovies: state.homeReducer.upcomingMovies,
  genres: state.setupReducer.genres,
  loadingNowPlayingMovies: state.homeReducer.loadingNowPlayingMovies,
  nowPlayingMovies: state.homeReducer.nowPlayingMovies,
  loadingPopularMovies: state.homeReducer.loadingPopularMovies,
  popularMovies: state.homeReducer.popularMovies,
  loadingTopRatedMovies: state.homeReducer.loadingTopRatedMovies,
  topRatedMovies: state.homeReducer.topRatedMovies,
  featured: state.homeReducer.featured,
});

const mapDispatchToProps = dispatch => ({
  getUpcomingMovies: () => getUpcomingMovies(dispatch),
  getGenres: () => getGenres(dispatch),
  getNowPlayingMovies: () => getNowPlayingMovies(dispatch),
  getPopularMovies: () => getPopularMovies(dispatch),
  getTopRatedMovies: () => getTopRatedMovies(dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
