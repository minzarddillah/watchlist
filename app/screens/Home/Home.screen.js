import React, {useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import Image from 'react-native-fast-image';

import styles from './Home.style';
import HeaderHome from '../../components/HeaderHome/HeaderHome.component';
import ListHorizontal from '../../components/ListHorizontal/ListHorizontal.component';
import Banner from '../../components/Banner/Banner.component';

const HomeScreen = ({
  getNowPlayingMovies,
  getUpcomingMovies,
  getGenres,
  loadingUpcoming,
  upcomingMovies,
  genres,
  loadingNowPlayingMovies,
  nowPlayingMovies,
  getPopularMovies,
  loadingPopularMovies,
  popularMovies,
  getTopRatedMovies,
  loadingTopRatedMovies,
  topRatedMovies,
  featured,
  navigation,
}) => {
  useEffect(() => {
    getUpcomingMovies();
    getGenres();
    getNowPlayingMovies();
    getPopularMovies();
    getTopRatedMovies();
  }, [
    getUpcomingMovies,
    getGenres,
    getNowPlayingMovies,
    getPopularMovies,
    getTopRatedMovies,
  ]);

  const goToListMovie = params => () => {
    navigation.navigate('ListMovie', params);
  };

  const onPressCard = data => {
    navigation.navigate('Detail', {data: {...data, type: 'Movie'}});
  };

  const onPressGenre = params => () => {
    navigation.navigate('ListMovie', params);
  };

  const onPressFeatured = params => () => {
    navigation.navigate('ListMovie', params);
  };

  return (
    <SafeAreaView style={styles.wrapper}>
      <HeaderHome />
      <ScrollView>
        <View style={styles.containerTitleSection}>
          <Text style={styles.titleSection}>Upcoming Movies</Text>
        </View>

        <Banner
          loading={loadingUpcoming}
          data={upcomingMovies}
          genres={genres}
          onPress={data => {
            navigation.navigate('Detail', {data: {...data, type: 'Movie'}});
          }}
        />

        <ListHorizontal
          title="Now Playing Movies"
          movies={nowPlayingMovies}
          onPressViewAll={goToListMovie({
            pathApiUrl: '/movie/now_playing',
            titleHeader: 'Now Playing Movies',
            type: 'Movie',
          })}
          loading={loadingNowPlayingMovies}
          testIDProps="seeAllNowPlayingMovies"
          onPressCard={onPressCard}
        />

        <View style={styles.containerTitleSection}>
          <Text style={styles.titleSection}>Explore by Genres</Text>
        </View>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.scrollviewHorizontal}>
          {genres.map(({id, name}) => (
            <TouchableOpacity
              key={id}
              style={styles.genre}
              onPress={onPressGenre({
                id,
                name,
                pathApiUrl: `/discover/movie?&with_genres=${id}`,
                titleHeader: name,
                type: 'Movie',
              })}>
              <Text style={styles.genreName}>{name}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>

        <ListHorizontal
          title="Popular Movies"
          movies={popularMovies}
          onPressViewAll={goToListMovie({
            pathApiUrl: '/movie/popular',
            titleHeader: 'Popular Movies',
            type: 'Movie',
          })}
          loading={loadingPopularMovies}
          testIDProps="seeAllPopularMovies"
          onPressCard={onPressCard}
        />

        <View style={styles.containerTitleSection}>
          <Text style={styles.titleSection}>Featured</Text>
        </View>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.scrollviewHorizontal}>
          {featured.map(
            ({backdrop_path, name, api, titleHeader, testID}, index) => (
              <TouchableOpacity
                key={index}
                style={styles.containerFeatured}
                testID={testID}
                onPress={onPressFeatured({
                  pathApiUrl: api,
                  titleHeader,
                  type: 'TV Series',
                })}>
                <Image
                  source={{
                    uri: `https://image.tmdb.org/t/p/w300${backdrop_path}`,
                  }}
                  resizeMode="cover"
                  style={styles.imageFeatured}
                />
                <View style={styles.shadowFeatured} />
                <Text style={styles.nameFeatured}>{name}</Text>
              </TouchableOpacity>
            ),
          )}
        </ScrollView>

        <ListHorizontal
          title="Top Rated Movies"
          movies={topRatedMovies}
          onPressViewAll={goToListMovie({
            pathApiUrl: '/movie/top_rated',
            titleHeader: 'Top Rated Movies',
            type: 'Movie',
          })}
          loading={loadingTopRatedMovies}
          testIDProps="seeAllTopRatedMovies"
          onPressCard={onPressCard}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

HomeScreen.defaultProps = {
  loadingUpcoming: false,
  upcomingMovies: [],
  genres: [],
  loadingNowPlayingMovies: false,
  nowPlayingMovies: [],
  loadingPopularMovies: false,
  popularMovies: [],
  loadingTopRatedMovies: false,
  topRatedMovies: [],
  featured: [],
};

export default HomeScreen;
