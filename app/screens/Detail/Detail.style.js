import {StyleSheet} from 'react-native';
import {getStatusBarHeight, ifIphoneX} from 'react-native-iphone-x-helper';

import color from '../../utils/color';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  contentContainerStyle: {
    width: '100%',
    paddingTop: getStatusBarHeight(true),
    flexGrow: 1,
  },
  backdropImage: {
    width: '100%',
    aspectRatio: 16 / 9,
    position: 'absolute',
    backgroundColor: color.CodGray,
  },
  containerClose: {
    height: 56,
    marginBottom: 40,
    justifyContent: 'center',
  },
  containerBackButton: {
    alignSelf: 'flex-end',
    marginRight: 14,
    backgroundColor: color.Black,
    padding: 6,
    borderRadius: 9999,
  },
  containerHeader: {
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  posterImage: {
    width: 120,
    aspectRatio: 0.69,
    borderRadius: 14,
    marginRight: 15,
  },
  contentHeader: {
    alignSelf: 'flex-end',
    flex: 1,
  },
  title: {
    color: color.Mercury,
    fontSize: 20,
    fontWeight: '600',
    marginBottom: 10,
  },
  wrapperDetailHeader: {
    marginBottom: 20,
  },
  containerDetailHeader: {
    flexDirection: 'row',
    marginBottom: 6,
  },
  rating: {
    flex: 1,
    color: color.Green,
  },
  textDetailHeader: {
    flex: 1,
    color: color.Mercury,
  },
  containerAction: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonAction: {
    alignItems: 'center',
  },
  containerDescription: {
    padding: 15,
  },
  overview: {
    color: color.Mercury,
    marginBottom: 20,
  },
  containerListCast: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginBottom: 10,
  },
  containerCast: {
    width: 40,
    height: 40,
    borderRadius: 40,
    marginRight: 15,
    backgroundColor: color.CodGray,
    marginBottom: 10,
    overflow: 'hidden',
  },
  profileCast: {
    width: '100%',
    height: '100%',
  },
  containerMoreCast: {
    width: 40,
    height: 40,
    borderRadius: 40,
    marginRight: 15,
    marginBottom: 10,
    overflow: 'hidden',
    borderWidth: 2,
    borderColor: color.White,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerTabBar: {
    borderBottomWidth: 2,
    borderColor: color.Tundora,
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  tab: {
    marginRight: 20,
  },
  titleTab: {
    color: color.Mercury,
    fontSize: 18,
    fontWeight: '500',
    marginBottom: 15,
    paddingHorizontal: 2,
  },
  activeTab: {
    width: '100%',
    height: 3,
    backgroundColor: color.Monza,
    borderRadius: 3,
  },
  containerContentTab: {
    marginTop: 20,
    flexGrow: 1,
  },
  containerTrailerList: {
    paddingHorizontal: 15,
    borderWidth: 1,
  },
  containerVideo: {
    width: '100%',
    aspectRatio: 16 / 9,
    borderRadius: 10,
    marginBottom: 30,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  thumbnailVideo: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    backgroundColor: color.CodGray,
  },
  shadowThumbnail: {
    backgroundColor: color.Black,
    width: '100%',
    height: '100%',
    opacity: 0.3,
    position: 'absolute',
  },
  buttonPlayVideo: {
    width: 45,
    height: 45,
    borderRadius: 45,
    borderWidth: 2,
    borderColor: color.White,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 4,
  },
  statusBar: {
    width: '100%',
    backgroundColor: color.Black,
    height: getStatusBarHeight() + ifIphoneX(20, 0),
    position: 'absolute',
    zIndex: 100,
  },
  wrapperModalContent: {
    backgroundColor: color.Black,
  },
  containerModalContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default styles;
