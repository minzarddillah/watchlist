import {connect} from 'react-redux';

import Detail from './Detail.screen';

import {getDetail} from '../../actions/DetailAction';
import {setSaved} from '../../actions/setupAction';

const mapStateToProps = state => ({
  cast: state.detailReducer.cast,
  loading: state.detailReducer.loading,
  videos: state.detailReducer.videos,
  similiar: state.detailReducer.similiar,
  detail: state.detailReducer.detail,
  genres: state.setupReducer.genres,
  saved: state.setupReducer.saved,
});

const mapDispatchToProps = dispatch => ({
  getDetail: params => getDetail(params, dispatch),
  setSaved: data => setSaved(data, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
