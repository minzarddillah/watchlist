import React, {useEffect, useState, useMemo, useRef} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  Linking,
  Animated,
  Share,
} from 'react-native';
import Image from 'react-native-fast-image';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import {InAppBrowser} from 'react-native-inappbrowser-reborn';
import Modal, {ModalContent, ScaleAnimation} from 'react-native-modals';
import _, {cloneDeep} from 'lodash';

import styles from './Detail.style';
import color from '../../utils/color';
import CardMovie from '../../components/CardMovie/CardMovie.component';
import {formatSeparator} from '../../utils/helper';
import Empty from '../../components/Empty/Empty.component';

const Detail = ({
  getDetail,
  route,
  navigation,
  // cast,
  loading,
  // videos,
  // similiar,
  genres,
  // detail,
  saved,
  setSaved,
}) => {
  const {
    backdrop_path,
    poster_path,
    vote_average,
    vote_count,
    type,
    release_date,
    overview,
    id,
    title,
    first_air_date,
    name,
    original_title,
  } = route?.params?.data || {};
  const [data, setData] = useState({
    detail: {},
    cast: [],
    videos: [],
    similiar: [],
  });
  const [tabIndex, setTabIndex] = useState(type === 'TV Series' ? 0 : 1);
  const [isShowMore, setIsShowMore] = useState(false);
  const [showStatusBar, setShowStatusBar] = useState(false);
  const [modalCast, setModalCast] = useState({});
  const [showPoster, setShowPoster] = useState(false);
  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    const paramsLogEvent = cloneDeep({
      id,
      title,
      original_title,
      type,
    });

    switch (tabIndex) {
      case 0:
        paramsLogEvent.tabName = 'Season';
        break;
      case 1:
        paramsLogEvent.tabName = 'Trailers';
        break;
      case 2:
        paramsLogEvent.tabName = 'More Like This';
        break;
      default:
        paramsLogEvent.tabName = '';
        break;
    }
  }, [id, original_title, tabIndex, title, type]);

  // useEffect(() => {
  //   load();
  // }, [load]);

  useEffect(() => {
    getDetail({type: type === 'Movie' ? 'movie' : 'tv', id}).then(response => {
      setData(response);
    });
  }, [getDetail, id, type]);

  const onPressBack = () => {
    navigation.goBack();
  };

  const onPressBrowse = async url => {
    try {
      if (await InAppBrowser.isAvailable()) {
        await InAppBrowser.open(url, {
          // IOS
          dismissButtonStyle: 'cancel',
          preferredBarTintColor: '#000',
          preferredControlTintColor: '#FFF',
          readerMode: true,
          animated: true,
          modalPresentationStyle: 'fullScreen',
          modalTransitionStyle: 'coverVertical',
          modalEnabled: true,
          enableBarCollapsing: true,

          // Android
          toolbarColor: '#000',
          secondaryToolbarColor: '#000',
          navigationBarColor: '#000',
          navigationBarDividerColor: '#FFF',
          showTitle: true,
          enableUrlBarHiding: true,
          enableDefaultShare: true,
          forceCloseOnRedirection: false,
          animations: {
            startEnter: 'slide_in_right',
            startExit: 'slide_out_left',
            endEnter: 'slide_in_left',
            endExit: 'slide_out_right',
          },
        });
      } else {
        Linking.openURL(url);
      }
    } catch (error) {
      Linking.openURL(url);
    }
  };

  const onPressSave = () => {
    setSaved([
      ...saved,
      {...(route?.params?.data || {}), type, createdAt: moment().toISOString()},
    ]);
  };
  const onPressRemoveSave = () => {
    const tmpSaved = [...saved].filter(e => e.id !== id);
    setSaved(tmpSaved);
  };

  const triggerMoreOrLess = () => {
    setIsShowMore(!isShowMore);
  };

  const onHideModal = () => {
    setModalCast({});
    return true;
  };

  const onPressShare = url => async () => {
    Share.share({
      message: `Flists - Watchlist Movie, TV Series and Anime\n\n${title}\n\n${overview}\n\n${url}`,
    });
  };

  const onScroll = event => {
    const positionY = event.nativeEvent.contentOffset.y;

    if (positionY >= 50 && !showStatusBar) {
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }).start();
      setShowStatusBar(true);
    } else if (positionY < 50 && showStatusBar) {
      setShowStatusBar(false);
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  };

  const {detail, cast, videos, similiar} = data;
  const isSaved = saved.find(e => e.id === id);

  const renderTrailers = useMemo(
    () => (
      <View style={styles.containerTrailerList}>
        {(videos?.length || 0) < 1 ? (
          <Empty title="No trailers to show" />
        ) : (
          videos?.map((video, index) => (
            <TouchableOpacity
              key={index}
              style={styles.containerVideo}
              onPress={() =>
                onPressBrowse(`https://www.youtube.com/watch?v=${video.key}`)
              }>
              <Image
                source={{
                  uri: `https://img.youtube.com/vi/${video.key}/mqdefault.jpg`,
                }}
                style={styles.thumbnailVideo}
                resizeMode="cover"
              />
              <View style={styles.shadowThumbnail} />
              <View style={styles.buttonPlayVideo}>
                <Icon name="play" size={24} color={color.White} />
              </View>
            </TouchableOpacity>
          )) || <></>
        )}
      </View>
    ),
    [videos],
  );

  const renderReleated = useMemo(
    () =>
      similiar?.map((item, index) => (
        <CardMovie item={item} genres={genres} type={type} key={index} />
      )) || <></>,
    [genres, similiar, type],
  );

  const renderSeasons = useMemo(
    () =>
      detail?.seasons
        ?.filter(({air_date, poster_path: posterPath}) => {
          if (!air_date || !posterPath) {
            return false;
          }
          return true;
        })
        ?.map((item, index) => (
          <CardMovie
            item={{
              ...item,
              overview:
                item.overview ||
                `Season ${item.season_number} of ${
                  title || name
                } premiered on ${moment(item.air_date, 'YYYY-MM-DD').format(
                  'DD MMMM YYYY',
                )}.`,
            }}
            genres={genres}
            type={type}
            key={index}
            subtitle={`Season ${item.season_number} • ${moment(
              item.air_date,
              'YYYY-MM-DD',
            ).format('YYYY')} • ${item.episode_count} Episodes`}
            disabled
            hideRating
          />
        )) || <></>,
    [detail?.seasons, genres, name, title, type],
  );

  return (
    <View style={styles.wrapper}>
      <Animated.View style={[styles.statusBar, {opacity: fadeAnim}]} />
      <ScrollView
        contentContainerStyle={styles.contentContainerStyle}
        onScroll={onScroll}
        scrollEventThrottle={16}>
        <Image
          source={{uri: 'https://image.tmdb.org/t/p/w500/' + backdrop_path}}
          style={styles.backdropImage}
        />
        <View style={styles.containerClose}>
          <TouchableOpacity
            onPress={onPressBack}
            style={styles.containerBackButton}>
            <Icon
              name="x"
              size={24}
              color={color.White}
              style={{transform: [{scale: 1.3}]}}
            />
          </TouchableOpacity>
        </View>
        <LinearGradient
          style={styles.containerHeader}
          colors={['rgba(0,0,0,0.0)', 'rgba(0,0,0,0.8)', 'rgba(0,0,0,1)']}>
          <TouchableOpacity onPress={() => setShowPoster(true)}>
            <Image
              source={{uri: 'https://image.tmdb.org/t/p/w300/' + poster_path}}
              style={styles.posterImage}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <View style={styles.contentHeader}>
            <Text style={styles.title}>{title || name}</Text>
            <View style={styles.wrapperDetailHeader}>
              <View style={styles.containerDetailHeader}>
                <Text style={styles.rating}>
                  {formatSeparator(vote_average, 1)} / 10 Rating
                </Text>
                <Text style={styles.textDetailHeader}>
                  {formatSeparator(vote_count)} Votes
                </Text>
              </View>
              <View style={styles.containerDetailHeader}>
                <Text style={styles.textDetailHeader}>{type}</Text>
                <Text style={styles.textDetailHeader}>
                  {moment(release_date || first_air_date, 'YYYY-MM-DD').format(
                    'MMM YYYY',
                  )}
                </Text>
              </View>
            </View>
            <View style={styles.containerAction}>
              <TouchableOpacity
                style={styles.buttonAction}
                onPress={isSaved ? onPressRemoveSave : onPressSave}>
                <IconAwesome
                  name={isSaved ? 'bookmark' : 'bookmark-o'}
                  size={24}
                  color={color.White}
                />
                <Text style={{color: color.Mercury}}>Save</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonAction}
                onPress={() => {
                  onPressBrowse(
                    detail?.homepage ||
                      `https://www.themoviedb.org/movie/${id}`,
                  );
                }}>
                <Icon name="globe" size={24} color={color.White} />
                <Text style={{color: color.Mercury}}>Browse</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonAction}
                onPress={onPressShare(
                  `https://www.themoviedb.org/movie/${id}`,
                )}>
                <Icon name="share" size={24} color={color.White} />
                <Text style={{color: color.Mercury}}>Share</Text>
              </TouchableOpacity>
            </View>
          </View>
        </LinearGradient>

        <View style={styles.containerDescription}>
          <Text style={styles.overview}>{overview}</Text>
          <View style={styles.containerListCast}>
            {loading
              ? Array.from(Array(3).keys()).map(index => (
                  <View style={styles.containerCast} key={index} />
                ))
              : [...cast]
                  .slice(0, isShowMore ? cast?.length || 0 : 3)
                  .map(({profile_path, original_name, character}, index) => (
                    <TouchableOpacity
                      key={index}
                      style={styles.containerCast}
                      onPress={() => {
                        setModalCast({profile_path, original_name, character});
                      }}>
                      <Image
                        source={{
                          uri: 'https://image.tmdb.org/t/p/w200' + profile_path,
                        }}
                        style={styles.profileCast}
                        resizeMode="cover"
                      />
                    </TouchableOpacity>
                  ))}
            {(cast?.length || 0) > 3 && (
              <TouchableOpacity
                style={styles.containerMoreCast}
                onPress={triggerMoreOrLess}>
                <Icon name="more-horizontal" size={24} color={color.White} />
              </TouchableOpacity>
            )}
          </View>
        </View>

        <View style={styles.containerTabBar}>
          {type === 'TV Series' && (
            <TouchableOpacity style={styles.tab} onPress={() => setTabIndex(0)}>
              <Text style={styles.titleTab}>Seasons</Text>
              {tabIndex === 0 && <View style={styles.activeTab} />}
            </TouchableOpacity>
          )}
          <TouchableOpacity style={styles.tab} onPress={() => setTabIndex(1)}>
            <Text style={styles.titleTab}>Trailers</Text>
            {tabIndex === 1 && <View style={styles.activeTab} />}
          </TouchableOpacity>
          <TouchableOpacity style={styles.tab} onPress={() => setTabIndex(2)}>
            <Text style={styles.titleTab}>More Like This</Text>
            {tabIndex === 2 && <View style={styles.activeTab} />}
          </TouchableOpacity>
        </View>
        <View style={styles.containerContentTab}>
          {(() => {
            if (tabIndex === 0) {
              return renderSeasons;
            } else if (tabIndex === 1) {
              return renderTrailers;
            } else if (tabIndex === 2) {
              return renderReleated;
            } else {
              return <View />;
            }
          })()}
        </View>
      </ScrollView>
      <Modal
        onTouchOutside={() => setShowPoster(false)}
        width={1}
        visible={showPoster}
        onSwipeOut={() => setShowPoster(false)}
        modalAnimation={new ScaleAnimation()}
        hasOverlay={false}
        style={{backgroundColor: color.Backdrop}}
        modalStyle={{backgroundColor: color.Transparent, alignItems: 'center'}}
        onHardwareBackPress={() => {
          setShowPoster(false);
          return true;
        }}>
        <TouchableWithoutFeedback onPress={() => setShowPoster(false)}>
          <Image
            source={{uri: 'https://image.tmdb.org/t/p/w500/' + poster_path}}
            style={{
              width: '95%',
              aspectRatio: 0.69,
              backgroundColor: color.CodGray,
              borderRadius: 20,
            }}
            resizeMode="cover"
          />
        </TouchableWithoutFeedback>
      </Modal>
      <Modal
        onTouchOutside={onHideModal}
        width={0.9}
        visible={!_.isEmpty(modalCast)}
        onSwipeOut={onHideModal}
        modalAnimation={new ScaleAnimation()}
        onHardwareBackPress={onHideModal}>
        <ModalContent style={styles.wrapperModalContent}>
          <TouchableWithoutFeedback onPress={onHideModal}>
            <View style={styles.containerModalContent}>
              <Image
                source={{
                  uri:
                    'https://image.tmdb.org/t/p/w200' +
                    (modalCast?.profile_path || ''),
                }}
                style={styles.posterImage}
                resizeMode="contain"
              />
              <View style={styles.wrapper}>
                <Text style={styles.title}>
                  {modalCast?.original_name || ''}
                </Text>
                <Text style={styles.overview}>
                  {modalCast?.character || ''}
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ModalContent>
      </Modal>
    </View>
  );
};

Detail.defaultProps = {
  getDetail: () => {},
};

export default Detail;
