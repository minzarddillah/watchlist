import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import color from '../../utils/color';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? getStatusBarHeight() : 0,
  },
  wrapperHeader: {
    paddingHorizontal: 20,
    height: 56,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  seperator: {
    height: 20,
  },
  loadingPagination: {
    marginVertical: 20,
  },
  inputSearch: {
    color: color.Mercury,
    flex: 1,
    height: '100%',
    paddingHorizontal: 10,
    borderRadius: 6,
    backgroundColor: color.CodGray,
    borderColor: color.MineShaft,
    borderWidth: 1,
    marginRight: 10,
  },
});

export default styles;
