import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
  Keyboard,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import debounce from 'lodash/debounce';

import styles from './Search.style';
import color from '../../utils/color';
import CardMovie from '../../components/CardMovie/CardMovie.component';
import Empty from '../../components/Empty/Empty.component';

const Search = ({
  page,
  totalPages,
  data,
  genres,
  search,
  requestSearchPaging,
  clearDataSearch,
  navigation,
}) => {
  const [keyword, setKeyword] = useState('');
  const [loadingSearch, setLoadingSearch] = useState(false);
  const [loadingPaging, setLoadingPaging] = useState(false);

  useEffect(() => () => {
    clearDataSearch();
  }, []);

  useEffect(() => {
    debounceSearch.current(keyword);
  }, [keyword]);

  const debounceSearch = useRef(
    debounce(searchKeyword => {
      if (searchKeyword.length > 1 && !loadingSearch) {
        setLoadingSearch(true);
        search(searchKeyword).finally(() => setLoadingSearch(false));
      }
    }, 1000),
  );

  const onEndReached = () => {
    if (loadingPaging || page > totalPages) {
      return true;
    }

    setLoadingPaging(true);
    requestSearchPaging({
      keyword,
      page,
    }).finally(() => setLoadingPaging(false));
  };

  const ListEmptyComponent = () => (
    <Empty
      title="No results to show"
      description={'Please check spelling or try\ndifferent keywords'}
      loading={loadingSearch}
    />
  );

  const renderItem = ({item}) => (
    <CardMovie
      item={item}
      genres={genres}
      type={item.media_type === 'movie' ? 'Movie' : 'TV Series'}
    />
  );

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.wrapper}>
        <View style={styles.wrapperHeader}>
          <TextInput
            value={keyword}
            onChangeText={setKeyword}
            style={styles.inputSearch}
            placeholder="Search TV Shows, Videos and Movies"
            placeholderTextColor={color.Tundora}
            autoFocus
          />
          <TouchableOpacity onPressIn={Keyboard.dismiss} onPressOut={navigation.goBack}>
            <Icon
              name="x"
              size={24}
              color={color.White}
              style={{transform: [{scale: 1.3}]}}
            />
          </TouchableOpacity>
        </View>
        {loadingSearch || data.length < 1 ? (
          <ListEmptyComponent />
        ) : (
          <FlatList
            data={loadingSearch ? [] : data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={renderItem}
            onEndReachedThreshold={0.2}
            onEndReached={onEndReached}
            onScroll={() => Keyboard.dismiss()}
            // onScrollBeginDrag={() => Keyboard.dismiss()}
            ListFooterComponent={() => {
              return loadingPaging ? (
                <ActivityIndicator
                  size="large"
                  color={color.White}
                  style={styles.loadingPagination}
                />
              ) : (
                <View style={styles.seperator} />
              );
            }}
          />
        )}
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

export default Search;
