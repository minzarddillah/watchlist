import {connect} from 'react-redux';

import Search from './Search.screen';
import {
  search,
  requestSearchPaging,
  clearDataSearch,
} from '../../actions/searchAction';

const mapStateToProps = state => ({
  page: state.searchReducer.page,
  totalPages: state.searchReducer.totalPages,
  data: state.searchReducer.data,
  genres: state.setupReducer.genres,
});

const mapDispatchToProps = dispatch => ({
  search: keyword => search(keyword, dispatch),
  requestSearchPaging: ({keyword, page}) =>
    requestSearchPaging({keyword, page}, dispatch),
  clearDataSearch: () => clearDataSearch(dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
