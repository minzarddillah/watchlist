import {StyleSheet, Platform} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';

import color from '../../utils/color';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? getStatusBarHeight() : 0,
  },
  title: {
    color: color.Monza,
    fontSize: 30,
    fontWeight: 'bold',
    margin: 20,
    marginBottom: 10,
  },
  containerFilter: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    marginHorizontal: 20,
  },
  buttonFilter: {
    backgroundColor: color.Mercury,
    paddingHorizontal: 10,
    paddingVertical: 4,
    marginRight: 10,
    borderRadius: 4,
    overflow: 'hidden',
  },
  activeButtonFilter: {
    backgroundColor: color.Monza,
  },
  activeTextFilter: {
    color: color.Mercury,
    fontWeight: '600',
  },
  inActiveTextFilter: {
    color: color.Black,
    fontWeight: '600',
  },
  divider: {
    width: '100%',
    height: 4,
    backgroundColor: color.CodGray,
  },
  contentContainerStyle: {
    flexGrow: 1,
    paddingTop: 20,
  },
});

export default styles;
