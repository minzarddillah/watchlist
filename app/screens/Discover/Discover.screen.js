import React, {useCallback, useMemo, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import moment from 'moment';

import styles from './Discover.style';
import CardMovie from '../../components/CardMovie/CardMovie.component';
import Empty from '../../components/Empty/Empty.component';

const DiscoverScreen = ({navigation, saved, genres}) => {
  const [filter, setFilter] = useState('');

  const onChangeFilter = useCallback(value => () => {
    setFilter(value);
  }, [setFilter]);

  const onPressEmptyButton = useCallback(
    () => navigation.navigate('Search'),
    [navigation],
  );

  const renderFilter = useMemo(() => (
    <View style={styles.containerFilter}>
      <TouchableOpacity
        style={[
          styles.buttonFilter,
          filter === '' ? styles.activeButtonFilter : {},
        ]}
        onPress={onChangeFilter('')}
      >
        <Text
          style={
            filter === ''
              ? styles.activeTextFilter
              : styles.inActiveTextFilter
          }
        >
          All
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.buttonFilter,
          filter === 'Movie' ? styles.activeButtonFilter : {},
        ]}
        onPress={onChangeFilter('Movie')}
      >
        <Text
          style={
            filter === 'Movie'
              ? styles.activeTextFilter
              : styles.inActiveTextFilter
          }
        >
          Movie
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.buttonFilter,
          filter === 'TV Series' ? styles.activeButtonFilter : {},
        ]}
        onPress={onChangeFilter('TV Series')}
      >
        <Text
          style={
            filter === 'TV Series'
              ? styles.activeTextFilter
              : styles.inActiveTextFilter
          }
        >
          TV Series
        </Text>
      </TouchableOpacity>
    </View>
  ), [filter, onChangeFilter]);

  const list = saved
    .sort((a, b) => moment(b.createdAt).diff(a.createdAt))
    .filter(e => {
      if (filter === 'Movie') {
        return e.type === 'Movie';
      } if (filter === 'TV Series') {
        return e.type === 'TV Series';
      }
      return true;
    });

  return (
    <SafeAreaView style={styles.wrapper}>
      <Text style={styles.title}>My Watchlist</Text>
      {renderFilter}
      <View style={styles.divider} />
      <ScrollView contentContainerStyle={styles.contentContainerStyle}>
        {list.length < 1 ? (
          <Empty
            title="You don't have a watchlist yet"
            description={
              'Add the movie to your watchlist and\nyou will never forget which movie\nit was you want to see'
            }
            onPressButton={onPressEmptyButton}
            textButton="Find Movies"
          />
        ) : (
          list.map((item, index) => (
            <CardMovie
              item={item}
              genres={genres}
              type={item.type}
              key={index}
            />
          ))
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default DiscoverScreen;
