import {connect} from 'react-redux';

import Discover from './Discover.screen';

const mapStateToProps = state => ({
  saved: state.setupReducer.saved,
  genres: state.setupReducer.genres,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Discover);
