import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import styles from './Header.style';
import color from '../../utils/color';

const Header = ({title}) => {
  const navigation = useNavigation();
  const onPressBack = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <View style={styles.containerTitle}>
        <Text style={styles.titleHeader}>{title}</Text>
      </View>
      <TouchableOpacity onPress={onPressBack}>
        <Icon
          name="chevron-left"
          size={24}
          color={color.White}
          style={styles.icon}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Header;
