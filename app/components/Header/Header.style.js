import {StyleSheet} from 'react-native';

import color from '../../utils/color';

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.Black,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 14,
    height: 56,
  },
  containerTitle: {
    position: 'absolute',
    top: 0,
    left: 14,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleHeader: {
    color: color.Monza,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '600',
    width: '100%',
  },
  icon: {
    transform: [{scale: 1.3}],
  },
});

export default styles;
