import {StyleSheet} from 'react-native';

import color from '../../utils/color';

const styles = StyleSheet.create({
  titleSection: {
    color: color.White,
    fontSize: 20,
    fontWeight: '600',
  },
  viewAll: {
    color: color.Monza,
    fontWeight: '600',
  },
  containerTitleSection: {
    paddingHorizontal: 20,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  scrollview: {
    paddingHorizontal: 20,
    marginBottom: 30,
  },
  containerCard: {
    width: 150,
    marginRight: 20,
  },
  posterCard: {
    width: '100%',
    aspectRatio: 0.69,
    borderRadius: 8,
    marginBottom: 8,
    backgroundColor: color.CodGray,
  },
});

export default styles;
