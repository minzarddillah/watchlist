import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import Image from 'react-native-fast-image';

import styles from './ListHorizontal.style';

const ListHorizontal = ({
  title,
  onPressViewAll,
  movies,
  loading,
  testIDProps,
  onPressCard,
}) => (
  <>
    <View style={styles.containerTitleSection}>
      <Text style={styles.titleSection}>{title}</Text>
      {onPressViewAll && (
        <TouchableOpacity onPress={onPressViewAll} testID={testIDProps}>
          <Text style={styles.viewAll}>View All</Text>
        </TouchableOpacity>
      )}
    </View>
    <ScrollView
      horizontal
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={styles.scrollview}>
      {loading
        ? Array.from(Array(3).keys()).map(index => (
            <View style={styles.containerCard} key={index}>
              <View style={styles.posterCard} />
              <Text style={styles.titleCard} />
            </View>
          ))
        : movies.map((data, index) => (
            <TouchableOpacity
              key={index}
              style={styles.containerCard}
              onPress={() => onPressCard(data)}>
              <Image
                source={{
                  uri: 'https://image.tmdb.org/t/p/w300/' + data.poster_path,
                }}
                resizeMode="cover"
                style={styles.posterCard}
              />
            </TouchableOpacity>
          ))}
    </ScrollView>
  </>
);

ListHorizontal.defaultProps = {
  title: '',
  movies: [],
  onPressCard: () => {},
};

export default ListHorizontal;
