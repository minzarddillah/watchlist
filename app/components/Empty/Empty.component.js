import React from 'react';
import {View, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import color from '../../utils/color';

import styles from './Empty.style';

const Empty = ({
  title = '',
  description = '',
  onPressButton = null,
  textButton = 'Button',
  loading = false,
}) => (
  <View style={styles.containerEmpty}>
    {loading ? (
      <ActivityIndicator size="large" color={color.White} />
    ) : (
      <>
        <Text style={styles.titleEmpty}>{title}</Text>
        <Text style={styles.descriptionEmpty}>{description}</Text>
        {onPressButton && (
          <TouchableOpacity
            style={styles.containerButtonEmpty}
            onPress={onPressButton}>
            <Text style={styles.textButtonEmpty}>{textButton}</Text>
          </TouchableOpacity>
        )}
      </>
    )}
  </View>
);

export default Empty;
