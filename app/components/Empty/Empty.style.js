import {StyleSheet} from 'react-native';

import color from '../../utils/color';

const styles = StyleSheet.create({
  containerEmpty: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleEmpty: {
    color: color.Mercury,
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 8,
    textAlign: 'center',
  },
  descriptionEmpty: {
    color: color.Mercury,
    fontSize: 18,
    textAlign: 'center',
  },
  containerButtonEmpty: {
    backgroundColor: color.Monza,
    paddingHorizontal: 14,
    paddingVertical: 8,
    borderRadius: 4,
    marginTop: 20,
  },
  textButtonEmpty: {
    color: color.Mercury,
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default styles;
