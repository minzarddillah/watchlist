import React from 'react';
import {View, Text, TouchableNativeFeedback} from 'react-native';
import Image from 'react-native-fast-image';
import Swiper from 'react-native-swiper';
import LinearGradient from 'react-native-linear-gradient';

import styles from './Banner.styles';

const Banner = ({
  loading, data, genres, onPress,
}) => (
  <View style={styles.wrapperBanner}>
    {loading ? (
      <View style={styles.containerBanner}>
        <View style={styles.imageBanner} />
      </View>
    ) : (
      <Swiper
        paginationStyle={styles.paginationStyle}
        dot={<View style={styles.dot} />}
        activeDot={<View style={styles.activeDot} />}
      >
        {data.map((item, index) => (
          <TouchableNativeFeedback
            onPress={() => onPress(item)}
            key={index}
          >
            <View style={styles.containerBanner}>
              <Image
                source={{
                  uri: `https://image.tmdb.org/t/p/w500/${item.backdrop_path}`,
                }}
                style={styles.imageBanner}
                resizeMode="cover"
              />
              <LinearGradient
                style={styles.containerDescriptionBanner}
                colors={[
                  'rgba(0,0,0,0.0)',
                  'rgba(0,0,0,0.7)',
                  'rgba(0,0,0,1)',
                ]}
              >
                <Text style={styles.typeBanner}>
                  Movies -
                  {' '}
                  {genres?.find(s => s.id === item.genre_ids[0])?.name}
                </Text>
                <Text style={styles.nameBanner} numberOfLines={1}>
                  {item.title}
                </Text>
              </LinearGradient>
            </View>
          </TouchableNativeFeedback>
        ))}
      </Swiper>
    )}
  </View>
);

Banner.defaultProps = {
  loading: false,
  data: [],
};

export default Banner;
