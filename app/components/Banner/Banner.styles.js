import {StyleSheet} from 'react-native';

import color from '../../utils/color';

const styles = StyleSheet.create({
  wrapperBanner: {
    height: 250,
  },
  containerBanner: {
    flex: 1,
    marginBottom: 40,
    justifyContent: 'flex-end',
    borderColor: color.White,
    paddingHorizontal: 20,
  },
  imageBanner: {
    left: 20,
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    backgroundColor: color.CodGray,
    borderRadius: 6,
  },
  paginationStyle: {
    left: 20,
    right: null,
  },
  dot: {
    backgroundColor: color.Tundora,
    width: 6,
    height: 6,
    borderRadius: 6,
    marginHorizontal: 4,
  },
  activeDot: {
    backgroundColor: color.Mercury,
    width: 8,
    height: 8,
    borderRadius: 8,
    marginHorizontal: 4,
  },
  containerDescriptionBanner: {
    padding: 10,
  },
  typeBanner: {
    color: color.Monza,
    fontSize: 10,
    fontWeight: '600',
  },
  nameBanner: {
    color: color.White,
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default styles;
