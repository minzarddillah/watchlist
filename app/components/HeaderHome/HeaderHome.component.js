import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import Image from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import styles from './HeaderHome.styles';
import logo from '../../assets/images/logo.png';
import color from '../../utils/color';

const HeaderHome = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Image source={logo} style={styles.logo} resizeMode="contain" />
      <TouchableOpacity onPress={() => navigation.navigate('Search')}>
        <Icon name="search" size={26} color={color.White} />
      </TouchableOpacity>
    </View>
  );
};

export default HeaderHome;
