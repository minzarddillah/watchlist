import React from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import Image from 'react-native-fast-image';
import moment from 'moment';
import {useNavigation} from '@react-navigation/native';

import styles from './CardMovie.style';
import {formatSeparator} from '../../utils/helper';

const CardMovie = ({
  item,
  genres,
  type,
  subtitle = null,
  disabled = false,
  hideRating = false,
}) => {
  const navigation = useNavigation();

  const onPressCard = () => {
    navigation.push('Detail', {data: {...item, type}});
  };

  return (
    <TouchableOpacity
      style={styles.containerItem}
      onPress={onPressCard}
      disabled={disabled}>
      <Image
        source={{uri: 'https://image.tmdb.org/t/p/w300' + item?.poster_path}}
        resizeMode="cover"
        style={styles.poster}
      />
      <View style={styles.containerContent}>
        <Text style={styles.titleItem} numberOfLines={2}>
          {item?.name || item?.title}
        </Text>
        <Text style={styles.typeItem} numberOfLines={1}>
          {subtitle ||
            `${type} (${moment(
              item?.first_air_date || item?.release_date,
              'YYYY-MM-DD',
            ).format('YYYY')}) •${item?.genre_ids?.map(
              id => ' ' + genres?.find(s => s.id === id)?.name,
            )}`}
        </Text>
        <Text style={styles.overviewItem} numberOfLines={3}>
          {item?.overview}
        </Text>
        {!hideRating && (
          <View style={styles.containerItemEnd}>
            <Text style={styles.ratingItem}>
              {formatSeparator(item?.vote_average, 1)}
              <Text style={styles.maxRatingItem}> / 10</Text>
            </Text>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default CardMovie;
