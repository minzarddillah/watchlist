import {StyleSheet} from 'react-native';

import color from '../../utils/color';

const styles = StyleSheet.create({
  containerItem: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  poster: {
    backgroundColor: color.CodGray,
    aspectRatio: 0.69,
    borderRadius: 8,
    flex: 1,
  },
  containerContent: {
    flex: 2,
    paddingLeft: 10,
    paddingTop: 4,
  },
  titleItem: {
    fontSize: 20,
    color: color.White,
    fontWeight: '600',
    marginBottom: 8,
  },
  typeItem: {
    fontSize: 12,
    color: color.White,
    fontWeight: '500',
    marginBottom: 8,
  },
  overviewItem: {
    fontSize: 12,
    color: color.Tundora,
    fontWeight: '500',
  },
  containerItemEnd: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  ratingItem: {
    fontSize: 20,
    color: color.White,
    fontWeight: '500',
  },
  maxRatingItem: {
    fontSize: 14,
  },
});

export default styles;
