import React from 'react';
import {StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import {ModalPortal} from 'react-native-modals';

import MainNavigation from './app/navigation/MainNavigation';
import store from './app/utils/store';

const App = () => (
  <Provider store={store}>
    <StatusBar
      translucent
      backgroundColor="transparent"
      barStyle="light-content"
    />
    <MainNavigation />
    <ModalPortal />
  </Provider>
);

export default App;
